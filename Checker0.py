#!/bin/python

import os # OS Module
import linecache # LineReader


# Line reader
def LineReader(filename,linenumber):
    line = linecache.getline(filename, linenumber)
    return line

output_value = LineReader("TEMP_FILE001.txt",1)
if "GUI" in output_value:
    os.system("./GUI.sh")
elif "FILE" in output_value:
    os.system("./FILE.sh")
