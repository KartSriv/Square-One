#!/bin/bash

# License FILE
FILE=$"LICENSE.txt"

# GNU License
zenity --text-info \
       --title="License" \
       --filename=$FILE \
       --checkbox="We know you haven't read all those stuff 😃"

./YeahOrNah.sh
