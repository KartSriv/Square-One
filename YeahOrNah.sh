#!/usr/bin/env bash

# Values
gedit_editor="GUI"
file_explorer="FILE"

# Reference: https://askubuntu.com/questions/478186/help-creating-a-messagebox-notification-with-yes-no-options-before-an-applicatio?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
if zenity --question --title="Choose a method" --text "There are two ways to add a script. \n1) You can choose the file \n2) You can paste or write the code in your default editor." --ok-label="Use my editor dude!" --cancel-label="You dumb! I've made the file already."; then
    echo $gedit_editor  >TEMP_FILE001.txt  # create/overwrite file
    python Checker.py
else
    echo $file_explorer  >TEMP_FILE001.txt  # create/overwrite file
    python Checker.py
fi
