#!/bin/bash

FILE="$(zenity --file-selection --title="Select a File")"
dt=$(date)
dt=$(echo ${dt// /_})
where=$(pwd)
cp "$FILE" "$where"
FILE=$(basename "$FILE")
mv $FILE BASHRC_SCRIPT_$dt.txt
cat BASHRC_SCRIPT_$dt.txt >> ~/.bashrc
./Stored.sh
