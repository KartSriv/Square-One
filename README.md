
<h1 align="center">
  <br>
  <a href="https://github.com/KartSriv/Square-One/"><img src="https://raw.githubusercontent.com/KartSriv/BrianAI/master/LogoMakr_5naH8G.png" alt="Markdownify" width="200"></a>
  <br>
   Square One (⬜ 1)
  <br>
</h1>
<h4 align="center">The universal <a href="https://www.pcmag.com/encyclopedia/term/52756/terminal-session" target="_blank">~/bashrc</a> editor.</h4>
  </a>
<h4 align="center">Run a Code Snippet or a Program at the Starting Point of every <a href="https://www.pcmag.com/encyclopedia/term/52756/terminal-session" target="_blank">Terminal Session</a>.</h4>
  </a>
  <a href="https://saythanks.io/to/KartSriv">
      <img src="https://img.shields.io/badge/SayThanks.io-%E2%98%BC-1EAEDB.svg">
  </a>
</p>

[![forthebadge made-with-python](http://ForTheBadge.com/images/badges/made-with-python.svg)](https://www.python.org/)
[![ForTheBadge built-with-science](http://ForTheBadge.com/images/badges/built-with-science.svg)](https://github.com/KartSriv/)
[![ForTheBadge built-with-love](http://ForTheBadge.com/images/badges/built-with-love.svg)](https://github.com/KartSriv/)
